//
//  AppDelegate.h
//  Practica1.1
//
//  Created by IDS Comercial on 03/10/17.
//  Copyright © 2017 IDS Comercial. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

