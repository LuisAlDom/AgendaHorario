//
//  AppDelegate.m
//  Practica1.1
//
//  Created by IDS Comercial on 03/10/17.
//  Copyright © 2017 IDS Comercial. All rights reserved.
//


#import "AppDelegate.h"
#import "RegistroDiaSemana+CoreDataClass.h"
#import "Recurso+CoreDataClass.h"
#import "Semana+CoreDataClass.h"

@interface AppDelegate ()
@property (nonatomic) NSManagedObjectContext *context; //Para que agarre
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {//Para que agarre el NSManaged
    self.context = self.persistentContainer.viewContext;
    [self createData];
    return YES;
}

-(void)createData{//Para que agarre
    Recurso *r1 = [[Recurso alloc] initWithContext: self.context];
    r1.idRecurso  = 1 ;
    r1.nombre = @"luis";
    r1.apPaterno = @"Dominguez";
    r1.apMaterno  = @"Borjas";
    r1.email = @"luis@gmail.com";
    
    Recurso *r2 = [[Recurso alloc] initWithContext: self.context];
    r2.idRecurso  = 2 ;
    r2.nombre = @"alberto";
    r2.apPaterno = @"Borjas";
    r2.apMaterno  = @"Rodriguez";
    r2.email = @"alejandro@gmail.com";
    [self saveContext];
}
/**
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
**/ //Para que agarre
#pragma mark - Core Data Stack

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {//para que agarre
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Practica1_1"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSError *error = nil;
    if ([self.context hasChanges] && ![self.context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
