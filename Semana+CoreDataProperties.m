//
//  Semana+CoreDataProperties.m
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "Semana+CoreDataProperties.h"

@implementation Semana (CoreDataProperties)

+ (NSFetchRequest<Semana *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Semana"];
}

@dynamic fechaFin;
@dynamic fechaInicio;
@dynamic idRecurso;
@dynamic idSemana;
@dynamic status;

@end
