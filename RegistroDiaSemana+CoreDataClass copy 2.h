//
//  RegistroDiaSemana+CoreDataClass.h
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegistroDiaSemana : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "RegistroDiaSemana+CoreDataProperties.h"
