//
//  Recurso+CoreDataClass.h
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Recurso : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Recurso+CoreDataProperties.h"
