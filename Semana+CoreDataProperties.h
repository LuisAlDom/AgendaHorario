//
//  Semana+CoreDataProperties.h
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "Semana+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Semana (CoreDataProperties)

+ (NSFetchRequest<Semana *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *fechaFin;
@property (nullable, nonatomic, copy) NSDate *fechaInicio;
@property (nullable, nonatomic, copy) NSString *idRecurso;
@property (nonatomic) int16_t idSemana;
@property (nonatomic) int16_t status;

@end

NS_ASSUME_NONNULL_END
