//
//  RegistroDiaSemana+CoreDataProperties.h
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "RegistroDiaSemana+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RegistroDiaSemana (CoreDataProperties)

+ (NSFetchRequest<RegistroDiaSemana *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *comentario;
@property (nullable, nonatomic, copy) NSDate *dia;
@property (nullable, nonatomic, copy) NSDecimalNumber *horas;
@property (nonatomic) int16_t idRegistroDiaSemana;
@property (nonatomic) int16_t idSemana;

@end

NS_ASSUME_NONNULL_END
