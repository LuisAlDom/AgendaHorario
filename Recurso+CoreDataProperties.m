//
//  Recurso+CoreDataProperties.m
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "Recurso+CoreDataProperties.h"

@implementation Recurso (CoreDataProperties)

+ (NSFetchRequest<Recurso *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Recurso"];
}

@dynamic apMaterno;
@dynamic apPaterno;
@dynamic email;
@dynamic idRecurso;
@dynamic nombre;

@end
