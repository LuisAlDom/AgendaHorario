//
//  Recurso+CoreDataProperties.h
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "Recurso+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Recurso (CoreDataProperties)

+ (NSFetchRequest<Recurso *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *apMaterno;
@property (nullable, nonatomic, copy) NSString *apPaterno;
@property (nullable, nonatomic, copy) NSString *email;
@property (nonatomic) int16_t idRecurso;
@property (nullable, nonatomic, copy) NSString *nombre;

@end

NS_ASSUME_NONNULL_END
