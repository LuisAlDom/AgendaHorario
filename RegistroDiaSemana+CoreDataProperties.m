//
//  RegistroDiaSemana+CoreDataProperties.m
//  
//
//  Created by IDS Comercial on 03/10/17.
//
//

#import "RegistroDiaSemana+CoreDataProperties.h"

@implementation RegistroDiaSemana (CoreDataProperties)

+ (NSFetchRequest<RegistroDiaSemana *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"RegistroDiaSemana"];
}

@dynamic comentario;
@dynamic dia;
@dynamic horas;
@dynamic idRegistroDiaSemana;
@dynamic idSemana;

@end
